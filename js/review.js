/* --------------------
		メルマガモーダル
-------------------- */
$(function(){
	// In
	$('#review_btn').click(function() {
		$('#modal_bg').fadeIn();
		$('#review_pop').fadeIn();
	});
	// Out
	$('#reviewClose').click(function() {
		$('#modal_bg').fadeOut('fast');
		$('#review_pop').fadeOut('fast');
	});
	$('#magazineButton_back').click(function() {
		$('#modal_bg').fadeOut('fast');
		$('#review_pop').fadeOut('fast');
	});
});