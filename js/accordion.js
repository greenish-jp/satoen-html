$(document).ready(function(){
	$('#accordion dt').click(function() {
		$(this).next('dd').slideToggle();
		$(this).next('dd').siblings('dd').slideUp();
    if($(this).hasClass("accordion") == false) {
      $(this).toggleClass('open');
      $(this).siblings('dt').removeClass('open');      
    }
	});
});
