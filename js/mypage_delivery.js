/* --------------------
		マイページ
-------------------- */
$(function(){
	// In
	$('.add_address').click(function() {
		$('.modal_bg').fadeIn();
		$('#modal_addAddress_wrapper').fadeIn();
	});
	// Out
	$('.cartClose').click(function() {
		$('.modal_bg').fadeOut('fast');
		$('#modal_addAddress_wrapper').fadeOut('fast');
	});
});