/* --------------------
		メルマガモーダル
-------------------- */
$(function(){
	// In
	$('#topA_liC').click(function() {
		$('#modal_bg').fadeIn();
		$('#mail_magazine_pop').fadeIn();
	});
	// Out
	$('#magazineClose').click(function() {
		$('#modal_bg').fadeOut('fast');
		$('#mail_magazine_pop').fadeOut('fast');
	});
	$('#magazineButton_back').click(function() {
		$('#modal_bg').fadeOut('fast');
		$('#mail_magazine_pop').fadeOut('fast');
	});
});