/* --------------------
		モーダルウィンドウ
-------------------- */
$(function(){
	// In
	$('#topB_liB').click(function() {
		$('#modal_bg').fadeIn();
		$('#modal_wrapper').fadeIn();
	});
	// Out
	$('#cartClose').click(function() {
		$('#modal_bg').fadeOut('fast');
		$('#modal_wrapper').fadeOut('fast');
	});
	$('#cartButton_back').click(function() {
		$('#modal_bg').fadeOut('fast');
		$('#modal_wrapper').fadeOut('fast');
	});
});





/* ------------------------------
		会員登録/ログインのフォーム起動
------------------------------ */
$(function(){
	// ログイン前
	$('#login').hover(function() {
		$('#login_child').fadeToggle('fast');
	});
	// ログイン後
	$('#mypage').hover(function() {
		$('#mypage_child').fadeToggle('fast');
	});
});
// マイページに切り替え
function test() {
	$('#login').hide();
	$('#mypage').fadeIn();
};