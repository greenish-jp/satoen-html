$(document).ready(function(){

	jQuery.preloadImages = function(){
    for(var i = 0; i<arguments.length; i++){
        jQuery("<img>").attr("src", arguments[i]);
    }
  };
  $.preloadImages('sp/img/common/header_menu_close.png');

	/* MENU */
	$('#headerA button').click(function() {
		$(this).toggleClass('open');
		$('#headerA > div').slideToggle('fast');
	});

	/* accordion */
	$('#accordion dt').click(function() {
		$(this).next('dd').slideToggle();
		$(this).next('dd').siblings('dd').slideUp();
		$('#accordion dd li span').siblings('ul').slideUp(); //
		$(this).toggleClass('open');
		$(this).siblings('dt').removeClass('open');

		// 位置調整
		var targetY = $('#aside_list').offset().top;
		$("html,body").animate({scrollTop:targetY});

	});
	$('#accordion dd li span').click(function() {
		$(this).next('ul').slideToggle();
		$(this).next('ul').siblings('ul').slideUp();
		$(this).toggleClass('open');
		//$(this).toggleClass('open');
		//$(this).siblings('dt').removeClass('open');
	});
});